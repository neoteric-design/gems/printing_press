# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# PrintingPress

Document export engine for the Neoteric Design CMS.

`printing_press` provides a reusable interface to [Pandoc](https://pandoc.org)
for generating PDFs, ePubs, and more from your models.

## Usage

If your object already responds to :title and :body it's as simple as

```ruby
PrintingPress.export(page, format: :pdf)
```

If not, don't fret! You can whip your own service object to expose the right
content. We call them Normalizers because they normalize the data and naming
things is hard. 

```ruby
class Thing < ApplicationRecord
end

...

# app/normalizers/thing_normalizer.rb
class ThingNormalizer << PrintingPress::Normalizer
  def body
    [field_1, field_2].join
  end

  def timestamp
    object.updated_at
  end

  def metadata
    { 
      author: object.author_name,
      date: timestamp
    }
  end
end

PrintingPress.export(thing, format: :pdf)

# Or if your normalizer class is something other than ModelNameNormalizer
PrintingPress.export(thing, format: :pdf, normalizer: FancyThingNormalizer)
```

PrintingPress only needs a title and body. Optionally, you can provide a
timestamp (or we'll use `Time.zone.now`), and metadata hash that will be [handed
off to Pandoc](http://pandoc.org/MANUAL.html#variables-set-by-pandoc).

### Adding additional export formats


TODO: explain

```ruby
module PrintingPress::Exporters
  class CustomExporter < Base
    export_format :custom
  end
end

```

### Normalizer

###
## Installation
Add this line to your application's Gemfile:

```ruby
gem 'printing_press'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install printing_press
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

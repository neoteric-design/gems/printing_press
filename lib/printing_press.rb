require 'printing_press/base_exporter'
require 'printing_press/file_export'
require 'printing_press/format_converter'
require 'printing_press/normalizer'

Dir[ Rails.root + 'printing_press/exporters/*_exporter.rb' ].each { |file| require file }

module PrintingPress
  def self.export(object, format:, normalizer: nil)
    exporter = exporter_for(format)
    normalizer ||= normalizer_for(object)
    normalized_document = normalizer.new(object)

    exporter.new(normalized_document).export
  end

  def self.exporter_for(format)
    "#{format.titleize}Exporter".constantize
  end

  def normalizer_for(object)
    guess = "#{object.class.name}Normalizer".constantize
    defined?(guess) ? guess : Normalizer
  end
end

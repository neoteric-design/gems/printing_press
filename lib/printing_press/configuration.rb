module PrintingPress
  class << self
    attr_reader :config

    def configure
      @config = Configuration.new
      yield config
    end
  end

  class Configuration
    def initialize
      @pandoc_options ||= {}
    end


    def pandoc_options
      default_pandoc_options.merge(@pandoc_options)
    end

    private

    def default_pandoc_options
      defaults = { from: :html }
      defaults['data-dir'] = ENV["PANDOC_DATA_DIR"] if ENV["PANDOC_DATA_DIR"].present?
      defaults
    end
  end
end
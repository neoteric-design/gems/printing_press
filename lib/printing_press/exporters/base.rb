module PrintingPress::Exporters
  class Base
    class << self
      attr_reader :format
      def export_format(format)
        @format = format
      end
    end

    attr_reader :document, :timestamp

    def initialize(normalized_document)
      raise FormatNotDefined if format.nil?
      @document = normalized_document
      @timestamp = (@document.timestamp || Time.now).to_s(:number)
    end

    def export
      puts formatted_content
    end

    def formatted_content
      format_converter.convert
    end

    def format_converter
      @format_converter ||= FormatConverter.new(full_text, format,
                                                formatting_options)
    end

    def formatting_options
      {}
    end

    def full_text
      document.header.to_s + document.body
    end

    def format
      self.class.format
    end
  end
end

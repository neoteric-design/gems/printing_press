module PrintingPress::Exporters
  class DocxExporter < Base
    include FileExport

    export_format :docx
  end
end

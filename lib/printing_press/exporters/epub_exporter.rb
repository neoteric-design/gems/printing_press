module PrintingPress::Exporters
  class EpubExporter < Base
    include FileExport

    export_format :epub
  end
end
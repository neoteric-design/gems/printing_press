module PrintingPress::Exporters
  class HtmlExporter < Base
    export_format :html
  end
end

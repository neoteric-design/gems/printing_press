module PrintingPress
  module FileExport
    def export
      output = format_converter.convert
      write_file(output) unless format_converter.wrote_to_file?

      full_path
    end

    def write_file(contents)
      File.open(full_path, 'wb') do |file|
        file << contents
      end
      full_path
    end

    def file_name
      "#{document.title}.#{timestamp}.#{file_ext}"
    end

    def file_ext
      format
    end

    def formatting_options
      { standalone: '', output: "\"#{full_path.to_s}\"" }
    end

    def full_path
      path_name + file_name
    end

    def path_name
      Rails.root + "tmp/"
    end
  end
end

module PrintingPress
  class FormatConverter
    attr_reader :text, :format, :options
    def initialize(text, format, **options)
      @text, @format, @options = text, format, options
    end

    def pandoc
      @pandoc ||= PandocRuby.new(text, pandoc_options)
    end

    def convert
      pandoc.convert
    end

    def wrote_to_file?
      !pandoc.binary_output
    end

    private

    def pandoc_options
      PrintingPress.config.pandoc_options.merge(to: format)
    end
  end
end

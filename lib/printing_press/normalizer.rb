require 'forwardable'

module PrintingPress
  class Normalizer
    extend Forwardable

    def initialize(object)
      @object = object
    end

    attr_reader :object
    def_delegators :@object, :title, :body

    def timestamp
      object&.updated_at || Time.zone.now
    end

    def metadata
      {}
    end

    def header
      output = "<head>"
      output << "<title>#{title}</title>"
      metadata.each do |k, v|
        output << "<meta name='#{k}'>#{v}</meta>"
      end
      output << "</head>"
    end
  end
end
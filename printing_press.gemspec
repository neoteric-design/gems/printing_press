$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "printing_press/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "printing_press"
  s.version     = PrintingPress::VERSION
  s.authors     = ["Madeline Cowie"]
  s.email       = ["madeline@cowie.me"]
  s.homepage    = "http://www.neotericdesign.com"
  s.summary     = "d"
  s.description = "d"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 4.2.5"
  s.add_dependency "pandoc-ruby", "~> 2.0"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
end
